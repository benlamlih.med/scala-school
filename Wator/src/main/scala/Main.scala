import scalafx.animation.Timeline.*
import scalafx.animation.{AnimationTimer, KeyFrame, Timeline}
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.application.{JFXApp3, Platform}
import scalafx.beans.property.{BufferProperty, IntegerProperty, ObjectProperty}
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Rectangle2D
import scalafx.scene.Scene
import scalafx.scene.paint.Color
import scalafx.scene.paint.Color.*
import scalafx.scene.shape.Rectangle
import scalafx.stage.Screen
import scalafx.util.Duration

import scala.collection.immutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Random

object WatorApp extends JFXApp3 {

  val generalBreedTime: Int     = 4
  var breedTimeReached: Boolean = false
  val sharkSize: Int            = 10
  val tunaSize: Int             = 10

  override def start(): Unit = {
    val nTunas: Int                                          = 300
    val nSharks: Int                                         = 100
    val screenBounds: Rectangle2D                            = Screen.primary.visualBounds
    val (boardWidth, boardHeight): (Int, Int)                =
      (screenBounds.width.intValue / sharkSize, screenBounds.height.intValue / sharkSize)
    val board: Array[Array[Option[Species]]]                 = Array.fill[Option[Species]](boardWidth, boardHeight)(None)
    val state: ObjectProperty[Array[Array[Option[Species]]]] = ObjectProperty(board)

    val tunas: List[Species]  = generateTunas(state, nTunas, boardWidth, boardHeight)
    val sharks: List[Species] = generateSharks(state, nSharks, boardWidth, boardHeight)

    // println("tunas: " + tunas)
    // println("sharks: " + sharks)

    tunas ++ sharks foreach { species => board(species.sX)(species.sY) = Some(species) }

    stage = new PrimaryStage {
      title = "Wator Simulation"
      width = boardWidth * sharkSize
      height = boardHeight * sharkSize
      scene = new Scene {
        fill = Black
        content = drawSpecies(state.value)
        state.onChange { (_, _, newState) =>
          content = drawSpecies(state.value)
        // println("in print: " + drawSpecies(state.value))
        }
      }
    }

    infiniteTimeline(state, boardWidth, boardHeight).play()
  }

  def generateTunas(state: ObjectProperty[Array[Array[Option[Species]]]], n: Int, width: Int, height: Int): List[Tuna] =
    List.fill(n) {
      val (x, y) = generateRandomPosition(state.value, width, height)
      Tuna(x, y)
    }

  def generateSharks(
      state: ObjectProperty[Array[Array[Option[Species]]]],
      n: Int,
      width: Int,
      height: Int
  ): List[Shark] =
    List.fill(n) {
      val (x, y) = generateRandomPosition(state.value, width, height)
      Shark(x, y)
    }

  def generateRandomPosition(board: Array[Array[Option[Species]]], width: Int, height: Int): (Int, Int) = {
    var x = Random.nextInt(width)
    var y = Random.nextInt(height)
    while (board(x)(y).isDefined) {
      x = Random.nextInt(width)
      y = Random.nextInt(height)
    }
    (x, y)
  }

  def drawSpecies(board: Array[Array[Option[Species]]]): List[Rectangle] = board.flatten.flatMap {
    case Some(species) => species.draw
    case None          => List.empty
  }.toList

  def infiniteTimeline(
      state: ObjectProperty[Array[Array[Option[Species]]]],
      boardWidth: Int,
      boardHeight: Int
  ): Timeline =
    new Timeline {
      keyFrames = List(
        KeyFrame(
          time = Duration(25),
          onFinished = _ => {
            updateState(state, boardWidth, boardHeight)
            breedTimeReached = state.value.flatten.count(_.isDefined) >= generalBreedTime
          }
        )
      )
      cycleCount = Indefinite
    }

  def updateState(state: ObjectProperty[Array[Array[Option[Species]]]], boardWidth: Int, boardHeight: Int): Unit = {
    val newBoard = Array.fill[Option[Species]](boardWidth, boardHeight)(None)
    for (i <- state.value.indices; j <- state.value(i).indices) {
      state.value(i)(j) match {
        case Some(tuna: Tuna)   =>
          val updatedTuna = tuna.move(state.value, i, j, boardWidth, boardHeight)
          updatedTuna match {
            case Some(newTuna) =>
              newBoard(newTuna.sX)(newTuna.sY) = Some(newTuna)
              if (newTuna.breedTimer == 0) newBoard(i)(j) = Some(Tuna(i, j)) // Create a new Tuna at the old position
            case None          => ()
          }
        case Some(shark: Shark) =>
          val updatedShark = shark.move(state.value, i, j, boardWidth, boardHeight)
          updatedShark match {
            case Some(newShark) =>
              newBoard(newShark.sX)(newShark.sY) = Some(newShark)
              if (newShark.breedTimer == 0) newBoard(i)(j) = Some(Shark(i, j)) // Create a new Shark at the old position
            case None           => ()
          }
        case _                  => ()
      }
    }
    state.update(newBoard) // Updating the state with newBoard.
  }

  import scala.util.Random

  sealed trait Species {
    def sX: Int

    def sY: Int

    def move(board: Array[Array[Option[Species]]], sX: Int, sY: Int, boardWidth: Int, boardHeight: Int): Option[Species]

    def draw: List[Rectangle]
  }

  case class Shark(sX: Int, sY: Int, energy: Int = 1000, breedTimer: Int = 0) extends Species {
    val breedTime = 15

    override def move(
        board: Array[Array[Option[Species]]],
        sX: Int,
        sY: Int,
        boardWidth: Int,
        boardHeight: Int
    ): Option[Shark] = {
      val directions = List((-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1))
        .map { case (dx, dy) => ((sX + dx + boardWidth) % boardWidth, (sY + dy + boardHeight) % boardHeight) }
      val tunaMoves  = directions.filter { case (x, y) => board(x)(y).exists(_.isInstanceOf[Tuna]) }
      val emptyMoves = directions.filter { case (x, y) => board(x)(y).isEmpty }

      val possibleMoves = if (tunaMoves.nonEmpty) tunaMoves else emptyMoves

      if (possibleMoves.nonEmpty) {
        val (newX, newY) = possibleMoves(Random.nextInt(possibleMoves.length))
        val isTuna       = board(newX)(newY).exists(_.isInstanceOf[Tuna])

        val newShark =
          copy(sX = newX, sY = newY, energy = if (isTuna) energy + 10 else energy - 1, breedTimer = breedTimer + 1)
        if (newShark.energy <= 0) None
        else if (breedTimer >= breedTime) Some(newShark.copy(breedTimer = 0))
        else Some(newShark)
      } else None
    }

    override def draw: List[Rectangle] = {
      val squareX = sX * sharkSize
      val squareY = sY * sharkSize
      List(new Rectangle {
        x = squareX.toDouble
        y = squareY.toDouble
        width = sharkSize
        height = sharkSize
        fill = Color.Red
      })
    }
  }

  case class Tuna(sX: Int, sY: Int, breedTimer: Int = 0) extends Species {
    val breedTime = 15

    override def move(
        board: Array[Array[Option[Species]]],
        sX: Int,
        sY: Int,
        boardWidth: Int,
        boardHeight: Int
    ): Option[Tuna] = {
      val directions = List((-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1))
        .map { case (dx, dy) => ((sX + dx + boardWidth) % boardWidth, (sY + dy + boardHeight) % boardHeight) }
      val emptyMoves = directions.filter { case (x, y) => board(x)(y).isEmpty }

      if (emptyMoves.nonEmpty) {
        val (newX, newY) = emptyMoves(Random.nextInt(emptyMoves.length))
        val newTuna      = copy(sX = newX, sY = newY, breedTimer = breedTimer + 1)

        if (breedTimer >= breedTime) Some(newTuna.copy(breedTimer = 0))
        else Some(newTuna)
      } else None
    }

    override def draw: List[Rectangle] = {
      val squareX = sX * tunaSize
      val squareY = sY * tunaSize
      List(new Rectangle {
        x = squareX.toDouble
        y = squareY.toDouble
        width = tunaSize
        height = tunaSize
        fill = Color.Green
      })
    }
  }
}
