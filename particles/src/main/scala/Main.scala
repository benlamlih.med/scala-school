package benlamlih.net

import jdk.internal.org.jline.reader.Widget
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.application.{JFXApp3, Platform}
import scalafx.beans.property.{IntegerProperty, ObjectProperty}
import scalafx.geometry.Rectangle2D
import scalafx.scene.Scene
import scalafx.scene.paint.Color
import scalafx.scene.shape.Circle
import scalafx.stage.Screen

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Random

object Main extends JFXApp3 {

  val PARTICLE_RADIUS                 = 3
  private val SPEED                   = 50
  private val NUMBER_OF_PARTICLES     = 1000
  private val BACKGROUND_COLOR: Color = Color.White
  private val WINDOW_NAME             = "Particle System"

  private def generateParticles(numberOfParticles: Int, width: Double, height: Double): List[Particle] = {

    def randomColor(): Color = {
      Color.rgb(Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
    }

    def randomDirection(): Direction = Random.shuffle(Direction.values.toList).head

    List.fill(numberOfParticles) {
      Particle(
        new Circle {
          centerX = Random.nextDouble() * width
          centerY = Random.nextDouble() * height
          radius = PARTICLE_RADIUS
          fill = randomColor()
        },
        randomDirection()
      )
    }
  }

  private def drawCircles(particles: List[Particle]): List[Circle] = particles.map(p => p.circle)

  private def gameLoop(update: () => Unit): Unit =
    def tick = Future {
      update()
      Thread.sleep(SPEED)
    }

    for {
      _ <- tick
      _ <- Future(gameLoop(update))
    } yield ()

  override def start(): Unit = {
    val primaryScreenBounds: Rectangle2D = Screen.primary.visualBounds
    val WINDOW_WIDTH: Double             = primaryScreenBounds.width
    val WINDOW_HEIGHT: Double            = primaryScreenBounds.height
    val frame                            = IntegerProperty(0) // game loop
    val particles                        = generateParticles(NUMBER_OF_PARTICLES, WINDOW_WIDTH, WINDOW_HEIGHT)
    val (relativeWidth, relativeHeight)  = (WINDOW_WIDTH.toInt / PARTICLE_RADIUS, WINDOW_HEIGHT.toInt / PARTICLE_RADIUS)
    val canvas                           = Array.ofDim[Boolean](relativeWidth + PARTICLE_RADIUS, relativeHeight + PARTICLE_RADIUS)
    val initialState                     = ObjectProperty(State(particles, WINDOW_WIDTH, WINDOW_HEIGHT, canvas))
    val p                                = ObjectProperty(initialState.value.particles)

    // updates the class State
    frame.onChange {
      initialState.update(initialState.value.update())
    }

    stage = new PrimaryStage {
      title = WINDOW_NAME
      width = WINDOW_WIDTH
      height = WINDOW_HEIGHT
      scene = new Scene {
        fill = BACKGROUND_COLOR
        content = drawCircles(particles)
        frame.onChange {
          content = drawCircles(p.value)
        }
      }
    }
    gameLoop(() => frame.update(frame.value + 1))
  }
}
