package benlamlih.net

import Main.PARTICLE_RADIUS

import scalafx.Includes.jfxPaint2sfx
import scalafx.collections.ObservableBuffer.Update
import scalafx.scene.SceneIncludes.jfxPaint2sfx
import scalafx.scene.paint.PaintIncludes.jfxPaint2sfx
import scalafx.scene.shape.Circle

import scala.math.{abs, floor}
import scala.util.Random

class State(var particles: List[Particle], width: Double, height: Double, canvas: Array[Array[Boolean]]) {

  def update(): State = {
    // Create a new canvas with all cells initially empty
    val updatedParticles = particles.map { p =>
      val (cX, cY)                     = (p.circle.centerX.value.toInt, p.circle.centerY.value.toInt)
      val (newX, newY)                 = calculateNextPosition(p.direction, cX, cY)
      val (relativeCX, relativeCY)     = (abs(floor(cX / PARTICLE_RADIUS)).toInt, abs(floor(cY / PARTICLE_RADIUS)).toInt)
      val (relativeNewX, relativeNewY) =
        (abs(floor(newX / PARTICLE_RADIUS)).toInt, abs(floor(newY / PARTICLE_RADIUS)).toInt)

      val isOutOfBounds    =
        newX - PARTICLE_RADIUS < 0 || newX + PARTICLE_RADIUS >= width.toInt || newY - PARTICLE_RADIUS < 0 || newY + PARTICLE_RADIUS >= height.toInt
      // Check if the new position is out of bounds
      val updatedDirection =
        if (isOutOfBounds || canvas(relativeNewX)(relativeNewY)) reverse(p.direction) else p.direction

      p.circle.centerX = newX
      p.circle.centerY = newY

      canvas(relativeCX)(relativeCY) = false
      canvas(relativeNewX)(relativeNewY) = true
      p.direction = updatedDirection

      p
    }
    new State(updatedParticles, width, height, canvas)
  }

  private def reverse(direction: Direction): Direction = {
    direction match {
      case Direction.UP         => Direction.DOWN
      case Direction.DOWN       => Direction.UP
      case Direction.LEFT       => Direction.RIGHT
      case Direction.RIGHT      => Direction.LEFT
      case Direction.UP_LEFT    => Direction.DOWN_RIGHT
      case Direction.UP_RIGHT   => Direction.DOWN_LEFT
      case Direction.DOWN_LEFT  => Direction.UP_RIGHT
      case Direction.DOWN_RIGHT => Direction.UP_LEFT
    }
  }

  private def calculateNextPosition(direction: Direction, cX: Int, cY: Int) = {
    direction match {
      case Direction.UP         => (cX, cY - PARTICLE_RADIUS)
      case Direction.DOWN       => (cX, cY + PARTICLE_RADIUS)
      case Direction.LEFT       => (cX - PARTICLE_RADIUS, cY)
      case Direction.RIGHT      => (cX + PARTICLE_RADIUS, cY)
      case Direction.UP_LEFT    => (cX - PARTICLE_RADIUS, cY - PARTICLE_RADIUS)
      case Direction.UP_RIGHT   => (cX + PARTICLE_RADIUS, cY - PARTICLE_RADIUS)
      case Direction.DOWN_LEFT  => (cX - PARTICLE_RADIUS, cY + PARTICLE_RADIUS)
      case Direction.DOWN_RIGHT => (cX + PARTICLE_RADIUS, cY + PARTICLE_RADIUS)
    }
  }
}
